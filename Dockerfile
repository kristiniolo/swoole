FROM php:alpine

MAINTAINER kharefa <kharefa@gmail.com>

ENV COMPOSER_ALLOW_SUPERUSER 1

# install modules : GD iconv
RUN apk update && apk upgrade && apk add --no-cache --virtual .dev-deps \
        freetype-dev \
        libjpeg-turbo-dev \
        libpng-dev \
        libssh-dev \
        pcre-dev \
        nghttp2-dev \
		autoconf && \
	apk add --no-cache --virtual .runtime-deps \
		procps \
        openssl \
        pcre \
        curl \
        wget \
        zip \
        unzip \
        git \
        vim && \
	docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ && \
    docker-php-ext-install \
    iconv \
    gd \
    pdo_mysql \
    mysqli \
    iconv \
    mbstring \
    json \
    opcache \
    sockets \
    pcntl && \
    echo "opcache.enable_cli=1" >> /usr/local/etc/php/conf.d/docker-php-ext-opcache.ini && \
	curl -sS https://getcomposer.org/installer | php && \
    mv composer.phar /usr/local/bin/composer && \
    composer self-update --clean-backups

# install swoole
RUN apk add --no-cache --virtual .buildBaseDeps build-base re2c  openssl-dev && \
	cd /root && \
    curl -o /tmp/swoole.tar.gz https://github.com/swoole/swoole-src/archive/master.tar.gz -L && \
    tar zxvf /tmp/swoole.tar.gz && cd swoole-src* && \
    phpize && \
    ./configure \
    --enable-openssl  \
    --enable-http2  \
    --enable-sockets \
    --enable-mysqlnd && \
    make && make install && \
    docker-php-ext-enable swoole && \
    echo "swoole.fast_serialize=On" >> /usr/local/etc/php/conf.d/docker-php-ext-swoole-serialize.ini && \
    rm -rf /tmp/* && \
	apk del .buildBaseDeps

# set Indonesia timezone
RUN echo 'Asia/Jakarta' > /etc/timezone && \
    printf "[Date]\ndate.timezone=Asia/Jakarta" > /usr/local/etc/php/conf.d/timezone.ini